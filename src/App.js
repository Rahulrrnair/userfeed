import React, { Component } from 'react';
import myData from './data.json';
import FeedContent from './feed';
import NewFeed from './newfeed';


class App extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
                        data:myData,
                      }
        this.handlesubmit = this.handlesubmit.bind(this);
        this.handlelike = this.handlelike.bind(this);
  }
  handlesubmit(user,comment)
  {
    
    var newfeed = { "user": user,
                    "value": comment,
                    "id": 1,
                    "timestamp": Date.now(),
                    "timeZoneOffset": "300",
                    "likes": 0
                  }
    var updatefeed = this.state.data.feed;
    updatefeed.unshift(newfeed)
    updatefeed = {'feed':updatefeed}
    this.setState({data:updatefeed})
    
  }
  handlelike(id)
  {
    alert(id)
  }
  render() {
   
    //var data=fs.readFileSync('data.json');
    return (
      <div className="App">

        <NewFeed onClickhandle={this.handlesubmit}/>

        <FeedContent data={this.state.data} onLikehandle={this.handlelike}/>
        
      </div>
    );
  }
}

export default App;

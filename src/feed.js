import React, { Component } from 'react';
import like from './like.svg';
class FeedContent extends Component {
  constructor(props) {
        super(props);
        this.state = {
                        data:this.props.data,
                      }
        
  }
  handlelike(key){
        
        this.state.data.feed[key].likes = this.state.data.feed[key].likes+1
        this.setState({data:this.state.data})
        

            }
  render() {
    var feedContent =  Object.values(this.state.data.feed).map((feed,key) => {
                        
                        var difference = Date.now() - feed.timestamp;
                        var daysDifference = Math.floor(difference/1000/60/60/24);
                        return (
                            <div>
                            <div className="feed-header">
                            <span className="feed-user">{feed.user?feed.user:'unknown user'}</span>
                            <span className="feed-likes">{feed.likes}<img src={like} className="feed-like" alt="like"  onClick={this.handlelike.bind(this,key)} /></span>
                            <span className="feed-days">{daysDifference} days ago</span>
                            </div>
                            <div className="feed-content">
                            <p className="feed-text">{feed.value}</p>
                            </div>
                            </div>
                        )
                        
                    })
    
    return (
      <div className='feed'>
        <p className="feed-count">{this.state.data.feed.length} feeds</p>
        {feedContent}
      </div>
    );
  }
}

export default FeedContent;

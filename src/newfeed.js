import React, { Component } from 'react';
class NewFeed extends Component {

    handlefeedSubmit(e){
        
        if(e.charCode==13)
        {
            this.props.onClickhandle(this.refs.user.value,this.refs.comment.value)
            this.refs.user.value ='';
            this.refs.comment.value ='';
        }
        
    }
  render() {
    return (
      <div className='feed'>
        <div class="form-group">
        <label for="user">User</label>
        <input className="form-control"  type="text" name="user" ref="user"/>
        </div>
        <div class="form-group">
        <label for="comment">Comment</label>
        <input className="form-control"  ref="comment" onKeyPress={this.handlefeedSubmit.bind(this)}/>
        </div>
       
      </div>
    );
  }
}

export default NewFeed;
